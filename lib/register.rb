module Register
  @in_definition = false
  @author = ''
  @source = ''
  @config = { name: '', description: '', repeatable: false,
                   script: { author: @author, source: @source }, type: :General,
                                                                    params: {} }
  Data = {}
  
  def self.notify(author, script)
    @author.clear
    @author << author
    @source.clear
    @source << script
  end
  
  def self.to_class_name(sym)
    "#{sym}".capitalize.gsub(/_(\w)/) { "_#{$1.upcase}" }
                                                     .gsub('__') { '::' }.to_sym
  end
  
  def self.method_missing(method, *args, &block)
    if @in_definition
      if method[/(.+)=/]
        @config[$1.to_sym] = (args.size > 1 ? args : args[0])
      elsif @config[method] then @config[method]
      else super(method, *args, &block) end
    else super(method, *args, &block) end
  end
  
  def self.add(klass = nil, &block)
    @in_definition = true
    params = (klass ? [[nil,klass]].concat(block.parameters) : block.parameters)
    keys = [to_class_name(params[0][1]), params[1][1]]
    keys.concat(params[2..-1].collect do |p|
     :"#{(p[0] == :rest ? '*' : '')}#{p[1]}"
    end)
    (Data[keys[0]] ||= {})[keys[1]] = [[*keys[2..-1]], @config, 
                                                          instance_exec(&block)]
    @config = { name: '', description: '', repeatable: false,
                   script: { author: @author, source: @source }, type: :General,
                                                                    params: {} }
    @in_definition = false
  end
end