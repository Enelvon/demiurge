module SES
  # SES::Demiurge
  # ---------------------------------------------------------------------------
  # Holds the version number.
  module Demiurge
  
    # The version number of SES::Demiurge. Used by {Updater}.
    VERSION = '1.0.0'
    
  end
end
