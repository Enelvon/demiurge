require 'fileutils'
require 'version'
require 'rubygems/package'
require 'zlib'

# Updater
# -----------------------------------------------------------------------------
# Performs automatic updates of Demiurge.
module Updater
  
  # Checks if an update is necessary.
  #
  # @return [void]
  def self.need_update?
    u = java.net.URL.new('https://raw.githubusercontent.com/sesvxace/' <<
                                               'demiurge/master/lib/version.rb')
    file = java.io.BufferedReader.new(java.io.InputStreamReader.new(
                                            u.open_connection.get_input_stream))
    version = '0.0.0'
    while (line = file.read_line) 
      version == $1 if line[/VERSION = '([\d\.]+)'/]
    end
    file.close
    
    if FileTest.directory?('updates') && FileTest.exist?('updates/version.rb')
      require './updates/version.rb'
    end
    
    return version if SES::Demiurge::VERSION < version
    return false
  end
  
  # Upgrades (or downgrades) to a given version of Demiurge.
  #
  # @param version [String] the version to which Demiurge should change
  # @return [void]
  def self.update(version)
    u = java.net.URL.new('https://github.com/sesvxace/demiurge/archive/' <<
                                                           "v#{version}.tar.gz")
    is = u.open_connection.get_input_stream.to_io
    File.open("v#{version}.tar.gz", 'w+b') do |f|
      IO.copy_stream(is, f)
    end
    is.close
    unpack_files("v#{version}")
    FileUtils.rm("v#{version}.tar.gz")
  end
  
  # Unpacks an installs an update archive.
  #
  # @param file [String] the name of the update archive
  # @return [void]
  def self.unpack_files(file)
    e = Gem::Package::TarReader.new(Zlib::GzipReader.open("#{file}.tar.gz"))
    e.rewind
    e.each do |entry|
      next unless entry.full_name[/lib/]
      if entry.directory?
        n = entry.full_name.sub("#{file}/") { '' }
        FileUtils.mkdir_p("updates/#{n[/lib\/(.+)/, 1]}")
      elsif entry.file?
        n = entry.full_name.sub("#{file}/") { '' }
        File.open("updates/#{n[/lib\/(.+)/, 1]}", 'w+') do |f|
          f.write(entry.read)
        end
      end
    end
    e.close
  end
end
