class TagTab < JPanel
  attr_reader :name
  
  def initialize(parent, owner, name, data_list, values)
    super(GridBagLayout.new)
    @parent = parent
    @owner = owner
    @name = name
    @data_list = data_list
    @values = values
    @contents = {}
    @to_init = []
    @constraints = GridBagConstraints.new
    reset_constraints(@constraints)
    @constraints.fill = GridBagConstraints::BOTH
    @constraints.gridwidth = GridBagConstraints::REMAINDER
    @constraints.weightx = 1
    @delete_item = ListDeleteAction.new
    @undo_actions = {}
    @redo_actions = {}
    @undo_managers = {}
    @undo_listener = UndoableEditListener.impl { |m,e|
      s = e.source
      @undo_managers[s].add_edit(e.edit)
      @undo_actions[s].update
      @redo_actions[s].update
    }
    build_contents(@values)
  end
  
  def build_contents(values)
    @values.sort_by { |k,v| v[1][:name] }.each do |p|
      k = p[0]
      v = p[1]
      panel = JPanel.new(GridBagLayout.new)
      c = GridBagConstraints.new
      reset_constraints(c)
      c.anchor = GridBagConstraints::CENTER
      text = JLabel.new("#{v[1][:name]} ")
      text.tool_tip_text = make_tool_tip(v[1][:description], v[1][:script])
      panel.add(text, c)
      if v[1][:repeatable]
        button = JButton.new('Add')
        button.tool_tip_text = 'Adds a copy of the tag.'
        button.add_action_listener(ActionListener.impl { |m,e|
          @contents[k][2].fill = GridBagConstraints::BOTH
          @contents[k][0].add(build_variable_panel(k, @contents[k][1]),
                                                                @contents[k][2])
          init_all
          @contents[k][2].gridy += 1
          @owner.scroll_panes[self].revalidate
        })
        c.gridy = 1
        c.anchor = GridBagConstraints::NORTH
        c.ipady = 0
        c.insets = Insets.new(-2, 0, 0, 0)
        panel.add(button, c)
        c.gridy = 0
        c.ipady = 4
        c.insets = Insets.new(2, 2, 2, 2)
        c.anchor = GridBagConstraints::CENTER
      end
      c.gridx = 1
      c.weightx = 1
      c.gridwidth = GridBagConstraints::REMAINDER
      c.fill = GridBagConstraints::BOTH
      panel.add(build_variable_panel(k, v), c)
      c.gridy += 1
      i = [1] * v[1][:params].keys.size
      @contents[k] = [panel, v, c, i]
      self.add(JScrollPane.new(panel), @constraints)
      @constraints.gridy += 1
    end
  end
  
  def build_variable_panel(key, variable_data)
    panel = JPanel.new(GridBagLayout.new)
    c = GridBagConstraints.new
    reset_constraints(c)
    variable_data[1][:params].values.each_with_index do |v,i|
      add_variable_components(key, i, panel, v, variable_data, c)
    end
    scroll_pane = JScrollPane.new(panel)
    if variable_data[1][:repeatable]
      c.anchor = GridBagConstraints::CENTER
      c.fill = GridBagConstraints::NONE
      button = JButton.new('Remove')
      button.add_action_listener(ActionListener.impl { |m,e|
        grid = @contents[key][0].get_layout
        i = grid.get_constraints(scroll_pane).gridy
        h1 = @parent.modified_data[@owner.name.to_sym] ||= {}
        h2 = ((h1[@owner.selection] ||= {})[key.to_sym] ||= [])
        h2[i] = nil
        h1[@owner.selection].delete(key.to_sym) if h2.empty?
        @owner.update_demi_data
        @parent.modified = true unless @first_run
        @contents[key][0].remove(scroll_pane)
        @owner.scroll_panes[self].revalidate
        @contents[key][0].repaint
      })
      panel.add(button, c)
    end
    scroll_pane
  end
  
  def add_variable_components(key, index, panel, v, variable_data, c)
    case v[0]
    when :Boolean
      add_label(panel, "#{v[1]} ", v[2], variable_data[1][:script], c)
      panel.add(build_bool_entry(key, index, v, variable_data[1][:script]), c)
    when :Float
      add_label(panel, "#{v[1]} ", v[2], variable_data[1][:script], c)
      panel.add(build_float_entry(key, index, v, variable_data[1][:script]), c)
    when :Integer
      add_label(panel, "#{v[1]} ", v[2], variable_data[1][:script], c)
      panel.add(build_integer_entry(key, index, v, variable_data[1][:script]),
                                                                              c)
    when :Paragraph
      c.gridheight = 5
      add_label(panel, "#{v[1]} ", v[2], variable_data[1][:script], c)
      panel.add(build_paragraph_entry(key, index, v,
                                                  variable_data[1][:script]), c)
      c.gridheight = 1
    when :String
      add_label(panel, "#{v[1]} ", v[2], variable_data[1][:script], c)
      panel.add(build_string_entry(key, index, v, variable_data[1][:script]), c)
    when :FloatArray, :IntegerArray, :StringArray
      c.gridheight = 5
      add_label(panel, "#{v[1]} ", v[2], variable_data[1][:script], c)
      panel.add(build_variable_list(key, index, v, variable_data[1][:script]),
                                                                              c)
      c.gridheight = 1
    end
    c.gridy += 1
  end
  
  def add_label(panel, name, help, author_data, constraints)
    constraints.anchor = GridBagConstraints::CENTER
    constraints.fill = GridBagConstraints::NONE
    constraints.gridwidth = 1
    constraints.gridx = 0
    constraints.weightx = 0
    label = JLabel.new(name)
    label.tool_tip_text = make_tool_tip(help, author_data)
    panel.add(label, constraints)
    constraints.gridx = 1
    constraints.fill = GridBagConstraints::BOTH
    constraints.gridwidth = GridBagConstraints::REMAINDER
    constraints.anchor = GridBagConstraints::WEST
    constraints.weightx = 1.0
    constraints.weighty = 1.0
  end
  
  def build_variable_list(key, index, variable_data, author_data)
    list = JList.new(DefaultListModel.new)
    list.input_map.put(KeyStroke.getKeyStroke("DELETE"), "delete_item")
    list.action_map.put("delete_item", @delete_item)
    list.cell_renderer = ListBackground.new
    list.selection_mode = ListSelectionModel::SINGLE_SELECTION
    5.times { list.model.add_element(' ') }
    list.tool_tip_text = make_tool_tip(variable_data[2], author_data)
    if variable_data[3]
      list.add_mouse_listener(MouseListener.impl { |m,e|
        unless @setting_data
          if @parent.project && m == :mouseClicked && e.click_count >= 2
            data = variable_data[3].call.compact
            if variable_data[5]
              data = data.collect { |v| variable_data[5].call(v) }.to_java
            else data = data.select { |v| !v.empty? }.to_java end
            if value = list_prompt(variable_data[1], variable_data[2], data)
              if list.selected_value == ' '
                if (ind = list.model.index_of(' ')) == list.model.size - 1
                  list.model.add(ind, value)
                  list.selected_index = ind
                else
                  list.model.remove(ind)
                  list.model.add(ind, value)
                  list.selected_index = ind
                end
              else
                ind = list.get_selected_index
                list.model.remove(ind)
                list.model.add(ind, value)
                list.selected_index = ind
              end
            end
          end
        end
      })
    else
      list.add_mouse_listener(MouseListener.impl { |m,e|
        unless @setting_data
          if @parent.project && m == :mouseClicked && e.click_count >= 2
            if value = value_prompt(*variable_data)
              if list.selected_value == ' '
                if (ind = list.model.index_of(' ')) == list.model.size - 1
                  list.model.add(ind, value)
                  list.selected_index = ind
                else
                  list.model.remove(ind)
                  list.model.add(ind, value)
                  list.selected_index = ind
                end
              else
                ind = list.get_selected_index
                list.model.remove(ind)
                list.model.add(ind, value)
                list.selected_index = ind
              end
            end
          end
        end
      })
    end
    i = (@contents[key] ? @contents[key][3][index] += 1 : 1) - 1
    list.model.add_list_data_listener(ListDataListener.impl { |m,e|
      unless @setting_data
        h1 = @parent.modified_data[@owner.name.to_sym] ||= {}
        h2 = ((h1[@owner.selection] ||= {})[key.to_sym] ||= [])
        h3 = (h2[i] ||= [])
        (h3[index] ||= []).clear
        list.model.size.times do |n|
          next if list.model.get(n).strip.empty?
          if (block = @values[key.to_sym][1][:params][:rest][4])
            h3[index] << block.call(list.model.get(n))
          else
            h3[index] << list.model.get(n)
          end
        end
        h3.delete_at(index) if h3[index].compact.empty?
        h2.delete_at(i) if h3.compact.empty?
        h1[@owner.selection].delete(key.to_sym) if h2.empty?
        @owner.update_demi_data
        @parent.modified = true unless @first_run
      end
    })
    list
  end
  
  def add_combo_box_listener_to(key, index, box)
    i = (@contents[key] ? @contents[key][3][index] += 1 : 1) - 1
    box.add_action_listener(ActionListener.impl { |m,e|
      unless @setting_data
        h1 = @parent.modified_data[@owner.name.to_sym] ||= {}
        h2 = ((h1[@owner.selection] ||= {})[key.to_sym] ||= [])
        h3 = (h2[i] ||= [])
        if box.selected_index == 0 && box.selected_item == ' '
          h3[index] = nil
          h2.delete_at(i) if h3.compact.empty?
          h1[@owner.selection].delete(key.to_sym) if h2.empty?
        else
          if (block = @values[key][1][:params][4])
            h3[index] = block.call(box.get_selected_item)
          else h3[index] = box.get_selected_item end
        end
        @owner.update_demi_data
        @parent.modified = true unless @first_run
      end
    })
  end
  
  def add_document_listener_to(key, index, field)
    i = (@contents[key] ? @contents[key][3][index] += 1 : 1) - 1
    field.document.add_document_listener(DocumentListener.impl { |m,e|
      unless @setting_data
        h1 = @parent.modified_data[@owner.name.to_sym] ||= {}
        h2 = ((h1[@owner.selection] ||= {})[key.to_sym] ||= [])
        h3 = (h2[i] ||= [])
        if m == :removeUpdate && field.get_text.empty?
          h3[index] = nil
          h2.delete_at(i) if h3.compact.empty?
          h1[@owner.selection].delete(key.to_sym) if h2.empty?
        else
          if (block = @values[key][1][:params][4])
            h3[index] = block.call(field.get_text)
          else h3[index] = field.get_text end
        end
        @owner.update_demi_data
        @parent.modified = true unless @first_run
      end
    })
    manager = UndoManager.new
    u = UndoAction.new(manager)
    r = RedoAction.new(manager)
    @undo_managers[field.document] = manager
    @undo_actions[field.document] = u
    @redo_actions[field.document] = r
    u.redo_action = r 
    r.undo_action = u
    field.input_map.put(KeyStroke.getKeyStroke("control Z"), 'undo')
    field.input_map.put(KeyStroke.getKeyStroke("control Y"), 'redo')
    field.action_map.put('undo', u)
    field.action_map.put('redo', r)
    field.document.add_undoable_edit_listener(@undo_listener)
  end
  
  def build_bool_entry(key, index, variable_data, author_data)
    entry = JCheckBox.new
    entry.add_item_listener(ItemListener.impl {
      unless @setting_data
        h1 = @parent.modified_data[@owner.name.to_sym] ||= {}
        h2 = (h1[@owner.selection] ||= {})
        if entry.selected then h2[key.to_sym] = [[true]]
        else h2.delete(key.to_sym) end
        @owner.update_demi_data
        @parent.modified = true unless @first_run
      end
    })
    entry.tool_tip_text = make_tool_tip(variable_data[2], author_data)
    entry
  end
  
  def build_float_entry(key, index, variable_data, author_data)
    if variable_data[3]
      entry = JComboBox.new
      add_combo_box_listener_to(key, index, entry)
      @to_init << [entry, variable_data[3], variable_data[5]]
      entry.tool_tip_text = make_tool_tip(variable_data[2], author_data)
      entry
    else
      field = TextField.new
      field.mode = 2
      add_document_listener_to(key, index, field)
      field.tool_tip_text = make_tool_tip(variable_data[2], author_data)
      field
    end
  end
  
  def build_integer_entry(key, index, variable_data, author_data)
    if variable_data[3]
      entry = JComboBox.new
      add_combo_box_listener_to(key, index, entry)
      @to_init << [entry, variable_data[3], variable_data[5]]
      entry.tool_tip_text = make_tool_tip(variable_data[2], author_data)
      entry
    else
      field = TextField.new
      field.mode = 1
      add_document_listener_to(key, index, field)
      field.tool_tip_text = make_tool_tip(variable_data[2], author_data)
      field
    end
  end
  
  def build_paragraph_entry(key, index, variable_data, author_data)
    field = JTextArea.new
    field.line_wrap = true
    field.wrap_style_word = true
    field.rows = 5
    add_document_listener_to(key, index, field)
    field.tool_tip_text = make_tool_tip(variable_data[2], author_data)
    field
  end
  
  def build_string_entry(key, index, variable_data, author_data)
    if variable_data[3]
      entry = JComboBox.new
      add_combo_box_listener_to(key, index, entry)
      @to_init << [entry, variable_data[3], variable_data[5]]
      entry.tool_tip_text = make_tool_tip(variable_data[2], author_data)
      entry
    else
      field = JTextField.new
      add_document_listener_to(key, index, field)
      field.tool_tip_text = make_tool_tip(variable_data[2], author_data)
      field
    end
  end
  
  def init_all
    @to_init.each do |c|
      data = [' '].concat(c[1].call.compact)
      if c[2]
        data = [' '].concat(data.collect { |v| c[2].call(v) }.to_java)
      else data = data.select { |v| !v.empty? }.to_java end
      c[0].model = DefaultComboBoxModel.new(data)
    end
    @to_init.clear
  end
  
  def make_tool_tip(help, author_data)
    "<html>Author: #{author_data[:author]}<br>" <<
                         "Source: #{author_data[:source]}<br><br>#{help}</html>"
  end
  
  def value_prompt(type, title, prompt, *ignore)
    @parent.show_input_dialog(title, prompt, type)
  end
  
  def list_prompt(title, prompt, list)
    @parent.show_choice_dialog(title, prompt, list)
  end
  
  def reset_constraints(constraints)
    constraints.gridx = 0
    constraints.gridy = 0
    constraints.gridwidth = 1
    constraints.gridheight = 1
    constraints.weightx = 0
    constraints.weighty = 0
    constraints.anchor = GridBagConstraints::WEST
    constraints.fill = GridBagConstraints::NONE
    constraints.ipady = 4
    constraints.insets = Insets.new(2, 2, 2, 2)
  end
  
  def removeAll
    super
    @parent = nil
    @owner = nil
    @data_list = nil
    @values = nil
    @constraints = nil
    @undo_listener = nil
    @undo_managers.clear
    @undo_actions.each_value { |u| u.clear }
    @undo_actions.clear
    @redo_actions.each_value { |r| r.clear }
    @redo_actions.clear
    @contents.clear
  end
  
  def remove_all(key)
    @setting_data = true
    @contents[key][2].gridy = 0
    @contents[key][3] = [0] * @values[key][1][:params].keys.size
    if @values[key][1][:repeatable]
      @contents[key][0].components
                                .select { |c| c.is_a?(JScrollPane) }
                                       .each { |p| @contents[key][0].remove(p) }
    else
      @contents[key][0].components
                                .select { |c| c.is_a?(JScrollPane) }
                            .collect { |c| c.viewport.components[0].components }
                                                         .flatten.each do |c|
        if c.is_a?(JList)
          c.model.remove_all_elements
          5.times { c.model.add_element(' ') }
          c.repaint
        elsif c.is_a?(JCheckBox)
          c.selected = false
        elsif c.is_a?(JComboBox)
          c.selected_index = 0
        elsif c.is_a?(JTextField) || c.is_a?(JTextArea) || c.is_a?(TextField)
          c.set_text('') unless c.is_a?(JTextArea)
          if c.is_a?(JTextArea)
            c.line_wrap = false
            c.set_caret_position(0)
            c.set_text('')
            c.line_wrap = true
          end
          c.revalidate
        end
      end
    end
    @undo_managers.each_value { |u| u.discard_all_edits }
    @setting_data = false
  end
  
  def reset_self
    init_all
    @contents.each_key { |k| remove_all(k) }
  end
  
  def set_data(tags)
    reset_self
    return unless tags
    @first_run = !(@run ||= [])[@owner.selection]
    if @first_run then @run[@owner.selection] = true
    else @setting_data = true end
    tags.each_pair do |k,v|
      panes = @contents[k][0].components.select { |c| c.is_a?(JScrollPane) }
                               .collect { |c| c.viewport.components[0] }.flatten
      v.each_with_index do |params, i|
        if @values[k][1][:repeatable]
          p = build_variable_panel(k, @contents[k][1])
          @contents[k][0].add(p, @contents[k][2])
          @owner.scroll_panes[self].revalidate
          @contents[k][2].gridy += 1
          panes << p.viewport.components[0]
          init_all
        end
        j = 0
        next unless panes[i]
        panes[i].components.each do |c|
          if c.is_a?(JScrollPane)
            c = c.viewport.components[0]
          end
          if c.is_a?(JList)
            c.model.remove_all_elements
            if @values[k][1][:params][:rest][3]
              container = @values[k][1][:params][:rest][3].call
              params[j..-1].each do |li|
                c.model.add_element(container[li.to_i])
              end
            else
              params[j..-1].each { |li| c.model.add_element("#{li}") }
            end
            if c.model.size < 5
              (5 - c.model.size).times { c.model.add_element(' ') }
            end
            j += 1
          elsif c.is_a?(JCheckBox)
            c.selected = true
            j += 1
          elsif c.is_a?(JComboBox)
            c.selected_item = params[j]
            j += 1
          elsif c.is_a?(JTextField) || c.is_a?(JTextArea) || c.is_a?(TextField)
            c.set_text(params[j])
            c.revalidate
            j += 1
          end
        end
      end
    end
    @undo_managers.each_value { |u| u.discard_all_edits }
    @setting_data = false
    @first_run = false
  end
end
