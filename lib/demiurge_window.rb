class DemiurgeWindow < com.github.sesvxace.demiurge.MainWindow
  attr_accessor :loaded, :closed
  attr_reader :data, :demi_data, :modified_data
  
  def initialize
    super
    @loading = false
    @data = {}
    @demi_data = {}
    @modified_data = {}
    @tabs = []
    #@modified = false
    create_dialogs
    if !self.project || self.project.empty?
      menu_item_save.setEnabled(false)
      menu_item_close.setEnabled(false)
    end
  end
  
  def tabs
    Register::Data.collect { |k,v| ["#{k}".pluralize, v] }
  end
  
  def add_constants
    @data.each_pair do |k,v|
      if k[/^Map(\d+)$/]
        v.instance_variable_set(:@id, $1.to_i)
        RPG::Map.send(:define_method, :id) do
          @id
        end
        v.instance_variable_set(:@name, @data[:MapInfos][v.id].name)
        RPG::Map.send(:define_method, :name) do
          @name
        end
        unless Object.const_defined?(:Data_Maps)
          @demi_data[:Maps] ||= []
          @modified_data[:Maps] ||= []
          @demi_data[:Events] ||= {}
          @modified_data[:Events] ||= {}
          Object.const_set(:Data_Maps, [])
          unless Object.const_defined?(:Data_Events)
            Object.const_set(:Data_Events, {})
          end
        end
        Data_Maps[v.id] = v
        @demi_data[:Maps][v.id] ||= v.note
        @demi_data[:Events][v.id] ||= {}
        Data_Events[v.id] = {}
        v.events.each_pair do |k,e|
          e.instance_variable_set(:@id, "#{v.id}_#{e.id}")
          Data_Events[e.id] = e
          @demi_data[:Events][e.id] ||= []
          e.pages.each_with_index do |p,i|
            @demi_data[:Events][e.id][i] ||= get_comments(p)
          end
        end
      else
        @demi_data[k] ||= []
        @modified_data[k] ||= []
        Object.const_set("Data_#{k}", v)
        if v.respond_to?(:'[]') && v[1] && v[1].respond_to?(:id) &&
                                                         v[1].respond_to?(:note)
          v.each { |i| @demi_data[k][i.id] ||= i.note if i }
        end
      end
    end
  end
  
  def add_tab(data)
    @tabs << DataTab.new(self, *data)
    tab_container.add(data[0], @tabs.last)
  end
  
  def create_choice_dialog
    @choice_dialog = [JOptionPane.new]
    p = JPanel.new(GridBagLayout.new)
    c = GridBagConstraints.new
    c.gridx = 0
    c.gridy = 0
    c.anchor = GridBagConstraints::WEST
    c.fill = GridBagConstraints::HORIZONTAL
    c.gridwidth = 2
    c.weightx = 1
    @choice_entry = JComboBox.new
    p.add(@choice_entry, c)
    c.insets = Insets.new(4, 4, 4, 4)
    c.ipadx = 4
    c.ipady = 4
    c.anchor = GridBagConstraints::CENTER
    c.gridwidth = 1
    c.gridy = 1
    c.weightx = 0
    c.fill = GridBagConstraints::NONE
    ok = JButton.new('OK')
    p.add(ok, c)
    cancel = JButton.new('Cancel')
    c.gridx = 1
    p.add(cancel, c)
    @choice_dialog[0].options = [p].to_java
    ok.add_action_listener(ActionListener.impl { 
      @choice_dialog[0].value = @choice_entry.selected_item
      @choice_dialog[1].visible = false
    })
    cancel.add_action_listener(ActionListener.impl {
      @choice_dialog[0].value = nil
      @choice_dialog[1].visible = false
    })
  end
  
  def create_dialogs
    create_choice_dialog
    create_input_dialog
  end
  
  def create_input_dialog
    @input_dialog = [JOptionPane.new]
    p = JPanel.new(GridBagLayout.new)
    c = GridBagConstraints.new
    c.gridx = 0
    c.gridy = 0
    c.anchor = GridBagConstraints::WEST
    c.fill = GridBagConstraints::HORIZONTAL
    c.gridwidth = 2
    c.weightx = 1
    @input_entry = TextField.new
    p.add(@input_entry, c)
    c.insets = Insets.new(4, 4, 4, 4)
    c.ipadx = 4
    c.ipady = 4
    c.anchor = GridBagConstraints::CENTER
    c.gridwidth = 1
    c.gridy = 1
    c.weightx = 0
    c.fill = GridBagConstraints::NONE
    ok = JButton.new('OK')
    p.add(ok, c)
    cancel = JButton.new('Cancel')
    c.gridx = 1
    p.add(cancel, c)
    @input_dialog[0].options = [p].to_java
    ok.add_action_listener(ActionListener.impl { 
      @input_dialog[0].value = @input_entry.text
      @input_dialog[1].visible = false
    })
    cancel.add_action_listener(ActionListener.impl {
      @input_dialog[0].value = nil
      @input_dialog[1].visible = false
    })
  end
  
  def create_tabs
    tabs.sort_by { |t| t[0] }.each { |tab| add_tab(tab) }
  end
  
  def recursive_component_iterate(component, &block)
    component.components.each { |c| recursive_component_iterate(c, &block) }
    yield component
  end
  
  def recursive_component_send(component, method)
    component.components.each { |c| recursive_component_send(c, method) }
    component.send(method) if component.respond_to?(method)
  end
  
  def closeProject
    return unless self.project
    if self.modified
      r = JOptionPane.showConfirmDialog(self, "Save changes to #{projectName}?",
                                  "Demiurge", JOptionPane::YES_NO_CANCEL_OPTION)
      return if r == JOptionPane::CANCEL_OPTION
      saveProject if r == JOptionPane::YES_OPTION
    end
    remove_tabs
    menu_item_save.setEnabled(false)
    menu_item_close.setEnabled(false)
    self.project = nil
    @closed = true
    self.title = 'Demiurge'
    remove_constants
    @data.clear
    @demi_data.clear
    @modified_data.clear
    @tabs.each do |tab|
      recursive_component_iterate(tab) do |t|
        t.disable if t.respond_to?(:disable)
      end
      tab.repaint
      tab.reset_all
    end
    self.modified = false
  end
  
  def get_comments(event)
    event.list.select { |c| c.code == 108 || c.code == 408 }.map! do |comment|
      comment.parameters.first
    end.join("\n")
  end
  
  def loadProject
    p = self.project
    closeProject
    self.project = p
    @loaded = true
    @demi_data.clear
    @modified_data.clear
    self.title = "#{project[/([^\/\\]+)$/, 1]} - Demiurge"
    load_project_files
    add_constants
    create_tabs
    @tabs.each do |tab|
      recursive_component_iterate(tab) do |t|
        t.enable if t.respond_to?(:enable)
      end
      tab.repaint
      tab.reload
    end
    menu_item_save.setEnabled(true)
    menu_item_close.setEnabled(true)
    self.modified = false
  end
  
  def load_project_files(skip_demiurge = false)
    @data.clear
    Dir.new("#{project}/Data").entries.each do |e|
      next if FileTest.directory?("#{project}/#{e}") || !e[/^\w+\.rvdata2$/]
      name = e[/(.+)\.rvdata2/, 1].to_sym
      File.open("#{project}/Data/#{e}", 'rb') do |data|
        if e == 'Demiurge.rvdata2'
          @demi_data = Marshal.load(data) unless skip_demiurge
        else @data[name] = Marshal.load(data) end
      end
    end
  end
  
  def projectName
    self.title[/(.+?) - Demiurge/, 1]
  end
  
  def reload
    remove_constants
    load_project_files(true)
    add_constants
    @tabs.each { |t| t.reset_list(true) }
  end
  
  def remove_constants
    @data.each_pair do |k,v|
      next unless Object.const_defined?("Data_#{k}")
      Object.send(:remove_const, "Data_#{k}")
    end
    Object.send(:remove_const, :Data_Maps) if Object.const_defined?(:Data_Maps)
  end
  
  def remove_tabs
    recursive_component_iterate(tab_container) do |c|
     c.removeAll if c.respond_to?(:removeAll)
     c.dispose if c.respond_to?(:dispose)
    end
    @tabs.clear
  end
  
  def saveProject
    return unless self.project
    unsorted_data = {}
    @demi_data.each_pair do |k,v|
      send("save_#{k.downcase}") if respond_to?("save_#{k.downcase}")
    end
    File.open("#{project}/Data/Demiurge.rvdata2", 'wb') do |file|
      Marshal.dump(@demi_data, file)
    end
    self.modified = false
  end
  
  def show_choice_dialog(title, prompt, list)
    @choice_entry.model = DefaultComboBoxModel.new([' '].concat(list).to_java)
    @choice_dialog[0].message = prompt
    @choice_dialog[1] = @choice_dialog[0].create_dialog(self, title)
    @choice_dialog[1].visible = true
    @choice_dialog[1] = nil
    @choice_dialog[0].value
  end
  
  def show_input_dialog(title, prompt, mode = 0)
    case mode
    when /string/i; mode = 0
    when /integer/i; mode = 1
    when /float/i; mode = 2
    end
    @input_entry.mode = mode
    @input_dialog[0].message = prompt
    @input_dialog[1] = @input_dialog[0].create_dialog(self, title)
    @input_dialog[1].visible = true
    @input_dialog[1] = nil
    @input_dialog[0].value
  end
end
